function ipAddressValidate(ip){
    if(ip == null || typeof ip != 'string'){
        return [];
    }
    ip = ip.split('.');
    if(ip.length === 4){
        for(let i = 0; i < ip.length; i++){
            let a = parseInt(ip[i])
            let b = ip[i]
            if(a == b && a>=0 && a<=255){
                ip[i] = parseInt(ip[i]);
            }else{
                return [];
            }
        }
        return ip
    }else{
        return []
    }
    

}


module.exports = ipAddressValidate;
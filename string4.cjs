function fullName(obj){
    if(obj == null || Object.keys(obj).length == 0){
        return ""
    }
    let name = "";
    if(obj.hasOwnProperty("middle_name")){
        let first_name = obj.first_name.toLowerCase();
        let middle_name = obj.middle_name.toLowerCase();
        let last_name =  obj.last_name.toLowerCase();
        first_name = first_name.replace(first_name[0], first_name[0].toUpperCase());
        middle_name = middle_name.replace(middle_name[0], middle_name[0].toUpperCase());
        last_name = last_name.replace(last_name[0], last_name[0].toUpperCase());
        name = first_name+' ' + middle_name+' ' +last_name;
    }else if(obj.hasOwnProperty("last_name")){
        let first_name = obj.first_name.toLowerCase();
        let last_name =  obj.last_name.toLowerCase();
        first_name = first_name.replace(first_name[0], first_name[0].toUpperCase());
        last_name = last_name.replace(last_name[0], last_name[0].toUpperCase());
        name = first_name+' ' +last_name;
    }else{
        let first_name = obj.first_name.toLowerCase();
        first_name = first_name.replace(first_name[0], first_name[0].toUpperCase());
        name = first_name;
    }
    return name;
}


module.exports = fullName;
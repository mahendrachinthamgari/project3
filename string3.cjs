function numberToMonth(date){
    if(date ==null || typeof date !== 'string'){
        return ''
    }
    obj = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'};
    let month;
    if(date.at(2) === '/'){
        if(date.at(4) === '/'){
            month = date.at(3);
        }else{
            month = date.at(3)+date.at(4);
        }
    }else{
        if(date.at(3) === '/'){
            month = date.at(2);
        }else{
            month = date.at(2)+date.at(3);
        }
    }    
    return obj[parseInt(month)];

}

module.exports = numberToMonth;
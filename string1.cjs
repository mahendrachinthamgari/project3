function convertToValue(str){
    if(str === null || typeof str !== 'string'){
        return 0;
    }
    str = str.replace('$', '');
    str = str.replaceAll(',', '');
    if(parseFloat(str) == str){
        str = parseFloat(str);
        return str
    }else{
        return 0;
    }
}


module.exports = convertToValue;
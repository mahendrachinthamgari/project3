function arrayToString(arr){
    if(arr == null || arr.length == 0 || !Array.isArray(arr)){
        return "";
    }
    return arr.join(' ');
}


module.exports = arrayToString;